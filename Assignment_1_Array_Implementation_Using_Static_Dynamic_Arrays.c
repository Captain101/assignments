#include <stdio.h> 
#include <stdlib.h> 
int *a; int *ptr[100];
int len=0; 
void addToSinglyLinkedList(int value) 
{ 
    a=calloc(1, sizeof(int)); 
    if(len==0) 
    { 
        a[len]=value; 
        ptr[len]=&a[len]; 
        len+=1;
    } 
    else 
    { 
        a[len]=value; 
        ptr[len]=&a[len]; 
        len+=1; 
    } 
} 
void displaySinglyLinkedList() 
{ 
    int i; 
    for(i=0;i<len-1;i++) 
    { 
        printf("%d->",*ptr[i]); 
    } 
    printf("%d",*ptr[i]); 
} 
void main()
{ 
    int choice,value; 
    do 
    { 
        printf("\n***************Linked List Implementation***************"); 
        printf("\n1. Add To Linked List \n2. Display Linked List \n3. Exit"); 
        printf("\n\nEnter choice: "); 
        scanf("%d",&choice); 
        switch(choice) 
        { 
            case 1: printf("\nEnter data: "); 
            scanf("%d",&value); 
            addToSinglyLinkedList(value); 
            break; 
            case 2: printf("\n\n---------Output--------------\n"); 
            displaySinglyLinkedList(); 
            printf("\n-----------------------------\n"); 
            break; 
            case 3: exit(0); 
            break; 
        } 
    }while(choice!=3); 
}