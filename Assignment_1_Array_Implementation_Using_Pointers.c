#include <stdio.h> 
#include <stdlib.h> 
struct node{ 
    int data; struct node *next; 
    }; 
struct node *head = NULL; 
struct node *tail = NULL; 
struct node *current = NULL; 
void addToSinglyLinkedList(int value) 
{ 
    current = malloc(sizeof(struct node)); 
    if(head==NULL) { current->data=value; 
    current->next=NULL; 
    head=tail=current; 
    } 
    else 
    { 
        current->data = value; 
        tail->next=current; 
        tail=current; current->next=NULL; 
    } 
} 
void displaySinglyLinkedList() 
{ 
    if(head==NULL) 
    { 
        printf("\n List is Empty"); 
    } 
    else 
    { 
        struct node *iter = head; 
        while(iter!=NULL) 
        { 
            printf("%d ",iter->data);
            iter = iter->next;
        } 
    } 
} 
void main()
{ 
    int choice,value; 
    do 
    { 
        printf("\n***************Linked List Implementation***************"); 
        printf("\n1. Add To Linked List \n2. Display Linked List \n3. Exit"); 
        printf("\n\nEnter choice: "); 
        scanf("%d",&choice); 
        switch(choice) 
        { 
            case 1: printf("\nEnter data: "); 
            scanf("%d",&value); 
            addToSinglyLinkedList(value); 
            break; 
            case 2: printf("\n\n---------Output--------------\n"); 
            displaySinglyLinkedList(); 
            printf("\n-----------------------------\n"); 
            break; 
            case 3: exit(0); 
            break; 
        } 
    }while(choice!=3); 
}