Q.1)	For each data structure, why do you think that data structure was needed? In other words,why would the inventor/inventors of a given structuring concept bother to create that concept in the first place? Why was inventing that structure so necessary?

A.1)	Binomial Heaps - A heap is a specialized tree-based data structure which is essentially an almost complete tree that satisfies the heap property: in a max heap, for any given node C, if P is a parent node of C, then the key (the value) of P is greater than or equal to the key of C. 
In a min heap, the key of P is less than or equal to the key of C. 
The node at the "top" of the heap (with no parents) is called the root node.
The heap is one maximally efficient implementation of an abstract data type called a priority queue, and in fact, priority queues are often referred to as "heaps", regardless of how they may be implemented. 
In a heap, the highest (or lowest) priority element is always stored at the root. However, a heap is not a sorted structure; it can be regarded as being partially ordered. 
A heap is a useful data structure when it is necessary to repeatedly remove the object with the highest (or lowest) priority. 
Heaps are also crucial in several efficient graph algorithms such as Dijkstra's algorithm.

Q.2)	Suppose you solve a problem using a certain data structure. What would happen to the solution if you used some other data structure? Note that the "new" solution may fail or may improve in performance or even degrade in performance.

A.2)	Suppose I solve a given problem using Binomial Heaps.The goal is to create a function that will rearrange data that continously flows into the databases. Huge amount of data keeps flowing everyday. There should be a priority given to this data as well. I will implement Binomial heap and use its union operation that has a time complexity of O(logN). So even under huge strain it will perform consistently. The incoming data need not be sorted.
	Suppose I now use Binary Heap to solve this problem. What are the roadblocks will I be running into? First thing would be Time complexity. With increase in the data the time complexity will keep degrading. Binary Heap has a time complexity of O(N). However the FindMIN Operation of it will be O(1). Here we are efficiently trading Find Operation speed for Merge operation speed.
	So using Binary Heap might not be best solution to problems that deal with large data.

Q.3)	We started the course by claiming that the essence of data structures is about :
(a) aggregating known structures to create a new one,
(b) containing information in various ways (linear, hierarchical and arbitrary), and
(c) constraining in various ways, e.g. LIFO on list for stack behaviour etc.
Critically analyse the data structures you have studied in this course and note the extent to which each one adheres to the above.

A.3)	Binomial Heaps - 	
	a) They are a composition of tree data structure that follow the MinHeap property. They do not follow the Binary tree rule , i.e each node can have at max 2 child nodes. It can have at max 2^K nodes. A Binomial Heap consist of multiple trees connected to each other. This connection resembles as a graph data structure.
	b) Binomial Heaps contain information that follows MinHeap property, i.e the root node always has lowest key value. Binomial Heap has multiple trees with multiple root nodes. In order to find minimum key, we need to go through roots of this multiple trees of the same heap.
It follows a parent child Hierarchy, that has left child, right sibling property.
	c) The most important use of Binomial Heaps is to merge multiple heaps in low time as possible. The nodes arranged in heap based upon their priorities. It efficiently is used for Priority Queues. They resemble each other so much that Priority Queues are mostly known as "Heaps".